package com.example.springbootTutorial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication //Ich bin dafür zuständig ein spring-boot-container zu starten
public class SpringBootTutorialApplication {

	/***
	 * main Method to bootstrap the Spring application (Stand-alone)
	 * */
	public static void main(String[] args) {
		SpringApplication.run(SpringBootTutorialApplication.class, args);
		// args sind die Command-Line Arguments that be passed to the Main Class, which
		// passes as well to the run Method
	}

}
