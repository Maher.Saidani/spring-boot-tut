package com.example.springbootTutorial.services;

import com.example.springbootTutorial.dtos.CourseDto;
import com.example.springbootTutorial.entities.Course;
import com.example.springbootTutorial.entities.Topic;
import com.example.springbootTutorial.repositories.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CourseService {

    public CourseRepository courseRepository;


    @Autowired
    public CourseService(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    public List<CourseDto> getAllCourseDtos(String topicId){

        List<CourseDto> courseDtos = new ArrayList<>();
        List<Course> courses = this.courseRepository.findByTopicId(topicId);

        for (Course course : courses){
            CourseDto courseDto = mapToDto(course);
            courseDtos.add(courseDto);
        }

        return courseDtos;
    }

    public CourseDto getCourseDto( String courseId){
        Course course = this.courseRepository.findById(courseId).get();
        return mapToDto(course);
    }

    public void postCourseDtos(List<CourseDto> courseDtos, String topicId) {

        for (CourseDto courseDto : courseDtos){
            Course course = mapToEntity(courseDto);
            course.setTopic(new Topic(topicId,"",""));
            System.out.println(course);
            this.courseRepository.save(course);
        }
    }

    public void deleteCourses() {
        this.courseRepository.deleteAll();
    }

    private Course mapToEntity(CourseDto courseDto) {

        Course course = new Course(
                courseDto.getId(),
                courseDto.getName(),
                courseDto.getDescription());

        return course;

    }

    private CourseDto mapToDto(Course course) {

        CourseDto courseDto = new CourseDto(
                course.getId(),
                course.getName(),
                course.getDescription(),
                course.getTopic()
        );

        return courseDto;
    }

    public boolean updateOneCourse(String courseId, CourseDto courseDto) {
        Course courseFromDB = courseRepository.findById(courseId).get();
        boolean successPut = courseFromDB != null;
        if (successPut){
            //courseFromDB.setId(courseDto.getId());
            courseFromDB.setDescription(courseDto.getDescription());
            courseFromDB.setName(courseDto.getName());

            courseRepository.save(courseFromDB);

        }

        return successPut;
    }

    public boolean deleteOneCourse(String courseId) {

        Course courseFromDB = courseRepository.findById(courseId).get();

        boolean successPut = courseFromDB != null;

        if (successPut){
            courseRepository.delete(courseFromDB);
        }

        return successPut;
    }
}
