package com.example.springbootTutorial.services;

import com.example.springbootTutorial.dtos.TopicDto;
import com.example.springbootTutorial.entities.Topic;
import com.example.springbootTutorial.repositories.TopicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TopicService {

    public TopicRepository topicRepository;


    @Autowired
    public TopicService(TopicRepository topicRepository) {
        this.topicRepository = topicRepository;
    }

    public List<TopicDto> getAllTopicDtos(){

        List<TopicDto> topicDtos = new ArrayList<>();
        List<Topic> topics = this.topicRepository.findAll();

        for (Topic topic : topics){
            TopicDto topicDto = mapToDto(topic);
            topicDtos.add(topicDto);
        }

        return topicDtos;
    }

    public TopicDto getTopicDto( String topicId){
        Topic topic = this.topicRepository.findById(topicId).get();
        return mapToDto(topic);
    }

    public void postTopicDtos(List<TopicDto> topicDtos) {

        for (TopicDto topicDto : topicDtos){
            Topic topic = mapToEntity(topicDto);
            this.topicRepository.save(topic);
        }
    }

    public void deleteTopics() {
        this.topicRepository.deleteAll();
    }

    private Topic mapToEntity(TopicDto topicDto) {

        Topic topic = new Topic(
                topicDto.getId(),
                topicDto.getName(),
                topicDto.getDescription());

        return topic;

    }

    private TopicDto mapToDto(Topic topic) {

        TopicDto topicDto = new TopicDto(
                topic.getId(),
                topic.getName(),
                topic.getDescription()
        );

        return topicDto;
    }

    public boolean updateOneTopic(String topicId, TopicDto topicDto) {
        Topic topicFromDB = topicRepository.findById(topicId).get();
        boolean successPut = topicFromDB != null;
        if (successPut){
            //topicFromDB.setId(topicDto.getTopicId());
            topicFromDB.setDescription(topicDto.getDescription());
            topicFromDB.setName(topicDto.getName());

            topicRepository.save(topicFromDB);

        }

        return successPut;
    }

    public boolean deleteOneTopic(String topicId) {

        Topic topicFromDB = topicRepository.findById(topicId).get();

        boolean successPut = topicFromDB != null;

        if (successPut){
            topicRepository.delete(topicFromDB);
        }

        return successPut;
    }
}
