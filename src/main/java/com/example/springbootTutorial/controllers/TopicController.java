package com.example.springbootTutorial.controllers;

import com.example.springbootTutorial.dtos.TopicDto;
import com.example.springbootTutorial.services.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TopicController {

    TopicService topicService;

    @Autowired
    public TopicController(TopicService topicService) {
        this.topicService = topicService;
    }

    @GetMapping("/topics")
    public List<TopicDto> getAllTopics(){
        return this.topicService.getAllTopicDtos();
    }

    @GetMapping("/topics/{topicId}")
    public TopicDto getTopic(@PathVariable("topicId") String topicId)
    {
        return this.topicService.getTopicDto(topicId);
    }

    @PostMapping("/topics")
    public void postTopics(@RequestBody List<TopicDto> topicDtos){
        this.topicService.postTopicDtos(topicDtos);
    }

    @PutMapping("/topics/{topicId}")
    public boolean updateOneTopic(
            @PathVariable("topicId") String topicId,
            @RequestBody TopicDto topicDto)
    {
        return this.topicService.updateOneTopic(topicId,topicDto);
    }

    @DeleteMapping("/delete/{topicId}")
    public boolean deleteOneTopic(@PathVariable("topicId") String topicId){
        return this.topicService.deleteOneTopic(topicId);
    }

    @DeleteMapping("/delete")
    public void deleteTopics(){
        this.topicService.deleteTopics();
    }

}
