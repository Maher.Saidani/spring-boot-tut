package com.example.springbootTutorial.controllers;

import com.example.springbootTutorial.dtos.CourseDto;
import com.example.springbootTutorial.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CourseController {

    CourseService courseService;

    @Autowired
    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping("/topics/{topicId}/courses")
    public List<CourseDto> getAllCourses(@PathVariable("topicId") String topicId){
        return this.courseService.getAllCourseDtos(topicId);
    }

    @GetMapping("/topics/{id}/courses/{courseId}")
    public CourseDto getCourse(@PathVariable("courseId") String courseId)
    {
        return this.courseService.getCourseDto(courseId);
    }

    @PostMapping("/topics/{topicId}/courses")
    public void postCourses(
            @RequestBody List<CourseDto> courseDtos,
            @PathVariable("topicId") String topicId){
        System.out.println(courseDtos);
        this.courseService.postCourseDtos(courseDtos, topicId);
    }

    @PutMapping("/topics/{id}/courses/{courseId}")
    public boolean updateOneCourse(
            @PathVariable("courseId") String courseId,
            @RequestBody CourseDto courseDto)
    {
        return this.courseService.updateOneCourse(courseId,courseDto);
    }

    @DeleteMapping("/topics/{id}/courses/delete/{courseId}")
    public boolean deleteOneCourse(@PathVariable("courseId") String courseId){
        return this.courseService.deleteOneCourse(courseId);
    }

    @DeleteMapping("/topics/{id}/courses/delete")
    public void deleteCourses(){
        this.courseService.deleteCourses();
    }

}
