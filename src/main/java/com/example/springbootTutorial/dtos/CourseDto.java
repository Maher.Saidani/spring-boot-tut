package com.example.springbootTutorial.dtos;

import com.example.springbootTutorial.entities.Topic;
import lombok.Data;

@Data
public class CourseDto {

    private String id;
    private String name;
    private String description;
    private Topic topic;

    public CourseDto() {
    }

    public CourseDto(String id, String name, String description) {
        this();
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public CourseDto(String id, String name, String description, Topic topic) {
        this();
        this.id = id;
        this.name = name;
        this.description = description;
        this.topic = topic;
    }



    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
