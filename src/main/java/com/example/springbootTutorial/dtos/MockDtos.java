package com.example.springbootTutorial.dtos;

import com.example.springbootTutorial.entities.Topic;

import java.util.Arrays;
import java.util.List;

public class MockDtos {

    public static List<TopicDto> getMockDtos(){
        List<TopicDto> topicDtos = Arrays.asList(

                new TopicDto(
                        "TS0",
                        "Typesript",
                        "Typescript basics"),

                new TopicDto(
                        "SpringB0",
                        "Spring boot",
                        "Spring-Boot basics"),

                new TopicDto(
                        "J0",
                        "Java",
                        "Java basics"),

                new TopicDto(
                        "Sql0",
                        "SQL",
                        "SQL basics")
        );

        return topicDtos;
    }

    public static Topic getMockTestTopic(){
        Topic topic = new Topic(
                "testId",
                "test",
                "Test basics");
        return topic;
    }

}
