package com.example.springbootTutorial.dtos;

import lombok.Data; //Auto Generation von getters and setters and toString() Methode

@Data
public class TopicDto {

    private String id;
    private String name;
    private String description;

    public TopicDto() {
    }

    public TopicDto(String id, String name, String description) {
        this();
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
