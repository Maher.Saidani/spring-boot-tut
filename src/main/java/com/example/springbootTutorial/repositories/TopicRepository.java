package com.example.springbootTutorial.repositories;

import com.example.springbootTutorial.entities.Topic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Database repository to access spring-boot-db schema.
 *
 * Automatically implements CRUD methods by Spring.
 *
 * Repository interface go as bean via dependency injection
 * into the context. No @Repository annotation needed.
 * Anyway, we put one for clarity sake.
 */

@Repository
public interface TopicRepository extends JpaRepository<Topic, String> {

//    @Query(value = "select t from Topic t where t.id = :topicId")
//    Topic findByTopicId(@Param("topicId") String topicId);





}
