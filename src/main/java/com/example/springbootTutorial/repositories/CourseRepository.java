package com.example.springbootTutorial.repositories;

import com.example.springbootTutorial.entities.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Database repository to access spring-boot-db schema.
 *
 * Automatically implements CRUD methods by Spring.
 *
 * Repository interface go as bean via dependency injection
 * into the context. No @Repository annotation needed.
 * Anyway, we put one for clarity sake.
 */

@Repository
public interface CourseRepository extends JpaRepository<Course, String> {

//    @Query(value = "select c from Course c where c.id = :courseId")
//    Course findByCourseId(@Param("courseId") String courseId);

    public List<Course> findByTopicId(String topicId);


}
