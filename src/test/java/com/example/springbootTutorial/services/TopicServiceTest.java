package com.example.springbootTutorial.services;


import com.example.springbootTutorial.dtos.MockDtos;
import com.example.springbootTutorial.dtos.TopicDto;
import com.example.springbootTutorial.entities.Topic;
import com.example.springbootTutorial.repositories.TopicRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * RunWith gives me a "fake" context which lives as long as the Test runs.
 * With this we can test our beans.
 */

@RunWith(SpringRunner.class)
@DataJpaTest
public class TopicServiceTest {

    @Autowired
    private TopicRepository topicRepository;

    @Autowired
    private TopicService topicService;

    @Mock
    private TopicRepository topicMockRepository;

    @InjectMocks //Jeder Mock wird in den serviece initieert
    private TopicService topicMockService;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void postTopicDtosTest() {

        //1. prepare test data
        List<TopicDto> topicDtos = MockDtos.getMockDtos();


        Topic topic = new Topic();
        when(topicMockRepository.save(any(Topic.class))).thenReturn(topic);

        //2. Run my business logik
        this.topicMockService.postTopicDtos(topicDtos);

        //3. verify result
        Mockito.verify(
                topicMockRepository,
                Mockito.times(topicDtos.size())).save(any(Topic.class)
        );
    }
}