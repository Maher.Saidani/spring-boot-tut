package com.example.springbootTutorial.repositories;


import com.example.springbootTutorial.dtos.MockDtos;
import com.example.springbootTutorial.entities.Topic;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * RunWith gives me a "fake" context which lives as long as the Test runs.
 * With this we can test our beans.
 */

@RunWith(SpringRunner.class)
@DataJpaTest
public class TopicRepositoryTest {

    @Autowired
    private TopicRepository topicRepository;

    @Test
    public void findByTopicIdTest(){

        //1. Prepare Test Data
        Topic topic = MockDtos.getMockTestTopic();

        //2. Run my business logik
        topicRepository.save(topic);

        //3. verify
        Topic topicFromDb = topicRepository.findById(topic.getId()).get();
        Assert.assertTrue(topicFromDb.getId() == topic.getId());
        Assert.assertTrue(topicFromDb.getName() == topic.getName());
        Assert.assertTrue(topicFromDb.getDescription() == topic.getDescription());

    }



}